# No Meet Reload - Chrome Extension

Chrome Extension that prevents erroneous reloads of Google Meet during a meeting

## Installation
Install the extension by:

1. download the extension from the downloads page (Download repository): [https://bitbucket.org/olivier-tille/no-meet-reload/downloads/](https://bitbucket.org/olivier-tille/no-meet-reload/downloads/)
2. unzip the downloaded Zip-File to a folder
3. if not already activated, activate the developer mode at: chrome://extensions
4. install the unpacked extentions through the "Load unpacked" button by selecting the folder from step 2.
